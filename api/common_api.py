#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   common_api.py
@Time    :   2021/01/13 13:04:06
@Author  :   Z20080498 
@Version :   1.0
@Contact :   
@WebSite :   
'''
# Start typing your code from here

from utils.common import json2dict, get_contents, dict2json_file
import os
import copy
import time
import datetime
import requests


class ICRoom():
    """
    20210129該類已經不再使用
    """
    def __init__(self, app=None):
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        self.host = app.config['CLIENT_HOST']
        self.port = app.config['CLIENT_PORT']
        self.client_id = app.config['CLIENT_ID']  # 访问物联⽹平台的应⽤ID,在物联⽹平台创建应⽤时由平台分配获得
        self.client_secret = app.config['CLIENT_SECRET']  # 访问物联⽹平台的应⽤密钥,与client_id对应，在物联平台中获取,或者联系商务⼈员获取

    @staticmethod
    def deviceid_to_meetingroom(deviceid):
        """
        deviceid_to_meeting = {"12M1":["1331339211"]}  # 设备信息对应会议室
        输入设备id,返回会议室名称
        """
        deviceid_to_meetingroom = {"1331339211":"12M1"}
        for k,v in deviceid_to_meetingroom.items():
            if deviceid == int(k):
                return v
        return ''

    @staticmethod
    def return_meeting_people(meeting):
        """
        meeting 会议对象query
        返回会议人员列表
        """
        sitinon = meeting.sitinon  # 列席人
        if sitinon:
            res = meeting.attendees.split(',') + sitinon.split(',')  # 所有会议人员的工号
        else:
            res = meeting.attendees.split(',')
        return res

    def headers(self):
        return {
            "Authorization": "Bearer {accessToken}".format(accessToken = self.access_token()),
            "content-type":"application/json;charset=UTF-8"
            }

    def door_control(self,doorid, expireTime="172800"):
        """
        門禁控制開關

        param doorid 門禁ID
        return 設備狀態和其他信息

        下发命令的状态。
        ● PENDING 表示缓存未下发
        ● EXPIRED  表示命令已经过期
        ● SUCCESSFUL  表示命令已经成功执⾏
        ● FAILED  表示命令执⾏失败
        ● TIMEOUT  表示命令下发执⾏超时
        ● CANCELED  表示命令已经被撤销执⾏
        ● DELIVERED  表示命令已送达设备
        ● SENT  表示命令正在下发
        """
        url = "http://{host}:{port}/api/v1/lock/doors/doorControl/{doorId}".format(host=self.host, port=self.port, doorId=doorid)
        data = {
            "status":"1",  # 控制門禁狀態
            "expireTime": expireTime
        }
        result = requests.post(url, data, headers=self.headers())
        if result.status_code == 200:
            res = result.json()
            return res
        else:
            return None
    
    def get_access_list(self, acId, size=10, current=1):
        """
        獲取門禁列表
        param acID 門禁控制器ID
        return json or None
        """
        url = "http://{host}:{port}/api/v1/lock/accessControllers/{acId}/doors?\
            size={size}&current={current}".format(host=self.host,port=self.port, acId=acId, size=size, current=current)
        result = requests.get(url,headers=self.headers())
        if result.status_code == 200:
            res = result.json()
            return res
        else:
            return None

    def get_access_controllers_list(self, size=10, current=1):
        """
        獲取門禁控制器列表
        return json數據 or None
        """
        url = "http://{host}:{port}/api/v1/lock/accessControllers?\
            size={size}&current={current}".format(host=self.host, port=self.port, size=size, current=current)
        result = requests.get(url, headers=self.headers())
        if result.status_code == 200:
            res = result.json()
            return res
        else:
            return None

    def access_token(self):
        """
        1. 獲取授權,將token寫入json文件,返回token
        2. 從文件獲取token,返回token
        3. 過期refres刷新token,寫入json,返回token
        return access_token or raise Error
        """
        # 存在緩存文件讀取文件判斷過期時間
        if os.path.isfile(os.path.join(os.getcwd(),'config/access_token.json')):
            access_token_config = json2dict(get_contents("config/access_token.json"))
            time_ = int(time.time()) - int(access_token_config.get("datetime", ''))
            # 小于1个半小时，直接读缓存文件
            if time_ < 3500:
            # if time_ < 30:  # 五分鐘后重新獲取
                # self.refresh_token = access_token_config['refresh_token']
                return access_token_config['access_token']

        url =  'http://{host}:{port}/api/v1/oauth/token'.format(host=self.host,port=self.port)
        # print(222,self.refresh_token)
        # if self.refresh_token:
        #     print(111,self.refresh_token)
        #     data = {
        #         "client_id": "%s" % self.client_id,
        #         "client_secret": "%s" % self.client_secret,
        #         "grant_type": "refresh_token",
        #         "refresh_token": self.refresh_token
        #     }
        #     result = requests.post(url, data)
        #     if result.status_code == 200:
        #         res = result.json()
        #         self.refresh_token = res.get("refresh_token")
        #         # token寫入文件
        #         print("已結超時,重新獲取token!")
        #         w_res = copy.deepcopy(res)
        #         w_res.update({"datetime":str(int(time.time()))})
        #         dict2json_file(w_res, file=os.join(os.getcwd(), 'config/access_token.json'))
        #         return res.get("access_token")
        #     else:
        #         raise GetAccessTokenError("獲取accessToken失敗!")
        # else:
        data = {
            "client_id": "%s" % self.client_id,
            "client_secret": "%s" % self.client_secret,
            "grant_type": "client_credentials"
        }
        result = requests.post(url, data)
        if result.status_code == 200:
            res = result.json()
            # token寫入文件
            w_res = copy.deepcopy(res)
            w_res.update({"datetime":str(int(time.time()))})
            dict2json_file(w_res, file=os.path.join(os.getcwd(), 'config/access_token.json'))
            return res.get("access_token")
        else:
            raise GetAccessTokenError("獲取accessToken失敗!")

    def subscriptions(self,deviceType,notifyType,callbackUrl,request='POST'):
        """
        訂閱平台業務數據
        param deviceType 設備類型 zigbee  LOCK
              notifyType 通知類型  deviceAdded         添加設備
                                   bindDevice          綁定設備
                                   deviceInfoChanged   設備信息變化
                                   lockCard            刷卡卡號上報
                                   lockQrcode          門禁二維碼上報
              callbackUrl  例如：http://XXX.XXX.XXX.XXX:443/callbackurltest
        
        return {
                    "subscriptionId": "*******",
                    "notifyType": "*******",
                    "callbackUrl": "http://***********"
                }
        """
        url = "http://{host}:{port}/api/v1/iot/subscriptions".format(host=self.host,port=self.port)
        data = {
            "deviceType":deviceType,
            "notifyType":notifyType,
            "callbackUrl":callbackUrl
        }
        if request == 'POST':
            result = requests.post(url,json=data,headers=self.headers())
        if request == 'GET':
            result = requests.get(url,headers=self.headers())
        if result.status_code == 200:
            res = result.json()
            return res
        else:
            return None

    def deleteSubscriptions(self,subscriptionId):
        """
        刪除單個訂閱
        param subscriptionId [str] 訂閱ID
        """
        url = "http://{host}:{port}/api/v1/iot/subscriptions/{subscriptionId}".format(host=self.host,port=self.port,subscriptionId=subscriptionId)
        result = requests.delete(url=url,headers=self.headers())
        if result.status_code == 200:
            return True
        else:
            return False