#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   views.py
@Time    :   2021/01/13 14:57:13
@Author  :   Z20080498 
@Version :   1.0
@Contact :   
@WebSite :   
'''
# Start typing your code from here

from flask import Blueprint, request, render_template, current_app, jsonify,abort
from flask.views import MethodView
import datetime
import os,json

# UDP協議
from udpclient import *
# 導入数据库模型
from app.models import SecurityEmployee as SE,HemmsMeetingSetupDetail as HMSD,HemmsMeetingCardRecode as HMCR
from extension import db

web_bpt = Blueprint('web_bpt',__name__,static_folder='static',template_folder='templates',static_url_path='web/static')

class LockCardView(MethodView):
    """
    門禁刷卡卡號回調地址,刷卡信息上報
    """
    def get(self):
        # 直接开门
        res = OpenTheDoor('1331339211')
        print(res)
        return jsonify({"msg":"OK!"}),200

    def post(self):
        res = request.get_json()
        if res.get('notifyType','').lower() == 'lockcard':  # 通知類型為刷卡卡號上報
            self.MainHandle(res)
        else:
            current_app.logger.warning("msg[通知类型为非刷卡卡號上報]")
        return jsonify({"msg":"OK"}),200


    def MainHandle(self,res):
        eventTimeStr = datetime.datetime.fromtimestamp(res['eventTime']).strftime('%Y-%m-%d %H:%M:%S')  # 刷卡时间
        cardid = res['service']['data']  # 八位卡號
        deviceid = res['deviceId']  # 设备id
        meetingroom = deviceid_to_meetingroom(deviceid)  # 设备ID转会议室名称
        notifytype = res['notifyType']  # 通知类型
        # 卡號查詢用戶信息
        user_info = SE.query.filter_by(cardid='%s' % cardid).first()
        if user_info:
            emplid = user_info.emplid  # 工號  10504060  
            # 刷卡信息记录
            hmcr = HMCR(emplid=emplid,eventtime=eventTimeStr,deviceid=deviceid,notifytype=notifytype)
            db.session.add(hmcr)
            db.session.commit()

            # 判断当前会议
            have_meeting = HMSD.query.filter(eventTimeStr>=HMSD.starttime\
                ).filter(eventTimeStr<=HMSD.endtime).filter(HMSD.status=='0' and HMSD.meetingroom==meetingroom).first()
            if have_meeting:
                if emplid in return_meeting_people(have_meeting):
                    # 有会议并且有权限
                    try:
                        OpenTheDoor(deviceid)
                        current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] 会议ID[{have_meeting.meetingid}] 设备状态[SUCCESS]')
                    except Exception as e:
                        current_app.logger.warning(f"工号[{emplid}] 设备ID[{deviceid}] 会议ID[{have_meeting.meetingid}] 设备状态[开门指令接口出错]")
                else:
                    current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] 会议ID[{have_meeting.meetingid}] msg[无权限]')
            else:
                current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] msg[当前无会议]')
            # 判断下一场会议
            next_meeting = HMSD.query.filter(\
                eventTimeStr <= HMSD.starttime).filter(HMSD.status=='0' and HMSD.meetingroom==meetingroom\
                ).order_by(HMSD.starttime).first()
            if next_meeting:
                if emplid in return_meeting_people(next_meeting):
                    # 当前时间大于开始时间减提醒时间
                    start_time = next_meeting.starttime  # 开始时间
                    eventTimeStr_ = datetime.datetime.strptime(eventTimeStr, "%Y-%m-%d %H:%M:%S")  # 刷卡时间
                    alertminute = int(next_meeting.alerttime)  # alertminute
                    alerttime = datetime.timedelta(minutes=alertminute)  # 提醒时间
                    alerttime_ = start_time - alerttime
                    if eventTimeStr_ >= alerttime_:  # 当前时间大于提前提醒时间
                        try:
                            OpenTheDoor(deviceid)
                            current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] 会议ID[{next_meeting.meetingid}] 设备状态[SUCCESS]')
                        except Exception as e:
                            current_app.logger.warning(f"工号[{emplid}] 设备ID[{deviceid}] 会议ID[{next_meeting.meetingid}] 设备状态[开门指令接口出错]")
                    else:
                        current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] 会议ID[{next_meeting.meetingid}] msg[下一场会议未开始]')
                else:
                    current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] 会议ID[{next_meeting.meetingid}] msg[无权限]')
            else:
                current_app.logger.info(f'工号[{emplid}] 设备ID[{deviceid}] msg[无下一场会议]')
        else:
            current_app.logger.warning(f"卡号[{cardid}] 设备ID[{deviceid}] msg[未查詢到該用戶]")

class ReachView(MethodView):
    """
    人員簽到情況展示
    """
    def get(self,deviceid):
        """
        設備id對應一個會議室.當有多個會議室時,以ID辨別
        deviceid 设备ID
        """
        meetingroom = deviceid_to_meetingroom(deviceid)
        if meetingroom == '':
            # 会议室限制
            abort(404)
        # 判断下一场会议
        eventTimeStr = '2021-01-20 14:00:00'
        # eventTimeStr = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')  # 请求时间
        next_meeting = HMSD.query.filter(eventTimeStr <= HMSD.starttime\
            ).filter(HMSD.status=='0' and HMSD.meetingroom==meetingroom\
            ).order_by(HMSD.starttime).first()
        if next_meeting:
            start_time = next_meeting.starttime  # 开始时间
            alerttime = datetime.timedelta(minutes=int(next_meeting.alerttime))  # alertminute
            eventTimeStr_ = datetime.datetime.strptime(eventTimeStr, "%Y-%m-%d %H:%M:%S")  # 请求时间
            alerttime_ = start_time - alerttime  # 提醒时间
            if alerttime_ <= eventTimeStr_ <= start_time:  # 当前时间在下一场会议的提醒时间内
                res = self.res(next_meeting)
                return render_template('web/reach.html',info=next_meeting,res=res)
            else:
                current_app.logger.info(f'会议ID[{next_meeting.meetingid}] msg[下一场会议未开始,继续判断当前会议]')

        # 判断当前会议
        have_meeting = HMSD.query.filter(eventTimeStr>=HMSD.starttime\
            ).filter(eventTimeStr<=HMSD.endtime).filter(HMSD.status=='0' and HMSD.meetingroom==meetingroom).first()
        if have_meeting:
            res = self.res(have_meeting)
            return render_template('web/reach.html',info=have_meeting,res=res)
        else:
            current_app.logger.info('msg[当前无会议且下一场会议未开始]')
            return render_template('web/reach.html',info={})

    def res(self,meeting):
        """
        meeting 会议对象(query对象)
        会议人员签到情况数据处理
        return res [{},{}] 该会议人员及签到状态
        """
        start_time = meeting.starttime  # 开始时间
        alerttime = datetime.timedelta(minutes=int(meeting.alerttime)) # alertminute
        alerttime_ = start_time - alerttime  # 提醒时间
        p_list = return_meeting_people(meeting)
        usersinfo = SE.query.filter(SE.emplid.in_(p_list)).all()
        # 签到表
        people_list = HMCR.query.filter(HMCR.eventtime >= alerttime_).filter(HMCR.eventtime <= meeting.endtime).all()
        res = []  # 所有会议人员信息和签到情况
        for i in range(len(p_list)):
            d = {}
            emplid = p_list[i]
            d['id'] = i+1  # 序号
            d['emplid'] = emplid  # 工号
            for user in usersinfo:  # 该工号的其他信息
                if emplid == user.emplid:
                    d['ename'] = user.ename
                    d['cname'] = user.cname
                    d['deptid'] = user.deptid  # 部門
                else:
                    d['ename'] = ''
                    d['cname'] = ''
                    d['deptid'] = ''


            for row in people_list:  # 该工号签到情况
                if emplid == row.emplid:
                    d['status'] = 1
                else:
                    d['status'] = 0
            res.append(d)
        return res

class IndexView(MethodView):
    """
    會議信息展示
    """
    def get(self):
        return render_template('web/index.html')

# 全局變量
class EnvView(MethodView):
    def get(self):
        return render_template('web/env.html')
web_bpt.add_url_rule(rule='/env/', view_func=EnvView.as_view('env'))





# 門禁刷卡卡號回調地址
web_bpt.add_url_rule(rule='/lockcard/', view_func=LockCardView.as_view('lockcard'))
# 簽到信息
# web_bpt.add_url_rule(rule='/', view_func=ReachView.as_view('/'))
web_bpt.add_url_rule(rule='/reach/<int:deviceid>/', view_func=ReachView.as_view('reach'), methods=['GET',])
# web_bpt.add_url_rule(rule='/reach/<int:meetingid>/', view_func=ReachView.as_view('reach_meetingid'), methods=['GET',])
# 會議信息展示
web_bpt.add_url_rule(rule='/index/', view_func=IndexView.as_view('index'))
