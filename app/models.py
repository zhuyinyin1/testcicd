# coding: utf-8
# from flask_sqlalchemy import SQLAlchemy


# db = SQLAlchemy()
from extension import db


class HemmsMeetingCardRecode(db.Model):
    __tablename__ = 'hemms_meeting_card_recode'

    emplid = db.Column(db.String(11), primary_key=True, info='工號')
    eventtime = db.Column(db.DateTime, nullable=False, info='刷卡時間')
    deviceid = db.Column(db.String(11), nullable=False, info='設備id')
    notifytype = db.Column(db.String(20), nullable=False, info='通知類型')



class HemmsMeetingSetupDetail(db.Model):
    __tablename__ = 'hemms_meeting_setup_detail'

    sysid = db.Column(db.String(18), nullable=False)
    meetingid = db.Column(db.String(18), primary_key=True, nullable=False)
    seq = db.Column(db.Integer, primary_key=True, nullable=False)
    starttime = db.Column(db.DateTime, nullable=False)
    endtime = db.Column(db.DateTime, nullable=False)
    status = db.Column(db.String(1), nullable=False)
    alerttime = db.Column(db.Integer, nullable=False, info='提醒時間')
    plant = db.Column(db.String(4), nullable=False)
    subject = db.Column(db.String(100), nullable=False)
    meetingroom = db.Column(db.String(30), nullable=False)
    host = db.Column(db.String(12), nullable=False)
    attendees = db.Column(db.String(4000))
    sitinon = db.Column(db.String(4000))



class SecurityEmployee(db.Model):
    __tablename__ = 'security_employee'

    emplid = db.Column(db.String(20), primary_key=True)
    cname = db.Column(db.String(50))
    ename = db.Column(db.String(50))
    plant = db.Column(db.String(5))
    mail = db.Column(db.String(50))
    deptid = db.Column(db.String(20))
    upper_dept = db.Column(db.String(20))
    empl_category = db.Column(db.String(10))
    supervisor = db.Column(db.String(20))
    officer_level = db.Column(db.String(3))
    cardid = db.Column(db.String(19))
    tdate = db.Column(db.String(8))
    treason = db.Column(db.String(30))
    udate = db.Column(db.DateTime)
    userid = db.Column(db.String(20))
    company = db.Column(db.String(3))
    deptn = db.Column(db.String(50))
    hdate = db.Column(db.DateTime)
    descrshort = db.Column(db.String(30))
    rehire_dt = db.Column(db.DateTime)
    adult = db.Column(db.String(1), server_default=db.FetchedValue())
    phone = db.Column(db.String(30))
    sex = db.Column(db.String(1))
