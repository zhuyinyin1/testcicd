#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   manage.py
@Time    :   2021/01/12 11:28:11
@Author  :   Z20080498 
@Version :   1.0
@Contact :   
@WebSite :   
'''
# Start typing your code from here


from flask_migrate import Migrate, MigrateCommand  # 數據庫遷移管理工具
from flask_script import Manager  # 添加一個外部執行方法
from create_app import create_app

# 導入程序實例app
app = create_app()
# 將擴展和實例綁定
from extension import db, bootstrap, logger
db.init_app(app)
bootstrap.init_app(app)
logger.init_app(app)

# 數據庫和app綁定
migrate = Migrate(app, db)

# 初始化Manager
manager = Manager(app)
# 添加 db 命令，并与 MigrateCommand 绑定
manager.add_command('db', MigrateCommand)

if __name__ == '__main__':
    manager.run()