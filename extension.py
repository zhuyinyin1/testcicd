from flask_sqlalchemy import SQLAlchemy
from flask_bootstrap import Bootstrap
from utils.logging import Logger


bootstrap = Bootstrap()
db = SQLAlchemy()
logger = Logger()
