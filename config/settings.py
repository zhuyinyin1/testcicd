class Config(object):
    from datetime import timedelta
    import os

    # 物聯網平台參數
    CLIENT_HOST = 'api.wookitech.com'  # 已經取消使用
    CLIENT_PORT = 80
    CLIENT_ID = '81821af9b31647bfa7e2ab4e61482e17'  # 由廠商提供
    CLIENT_SECRET = '312dbb7dc93f8346a102d'  # 由廠商提供

    DEBUG = False
    TESTING = False

    # 动态追踪
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    # 显示原始SQL语句
    SQLALCHEMY_ECHO = False
    # 设置session密钥
    SECRET_KEY = '3CoJUm6iyw5W7jud'
    SESSION_COOKIE_NAME = 'Yann-Zhu-WZS-P8'
    PERMANENT_SESSION_LIFETIME = timedelta(hours=12)  # 过期时间12小时
    
    # bootstrap-flask設置使用本地資源文件
    BOOTSTRAP_SERVE_LOCAL = True

    # flask-sqlchemy分頁不報錯
    SQLALCHEMY_CLOSE_DEBUG = True
    

class ProductionConfig(Config):
    # flask-sqlacodegen postgresql://FATP:FATP@10.41.158.153:5432/XXX --tables security_employee,hemms_meeting_setup_detail,hemms_meeting_card_recode --outfile 'app/models.py' --flask
    # 反向遷移時改為:from extension import db
    # SQLALCHEMY_DATABASE_URI = 'postgresql://FATP:FATP@10.41.158.153:5432/SABU_DB'
    pass


class DevelopmentConfig(Config):
    DEBUG = True
    # 反向遷移時需要使用該[命令],注意會覆蓋models.py,多個表使用逗號分隔
    # flask-sqlacodegen postgresql://FATP:FATP@10.41.158.153:5432/SABU_DB --tables security_employee,hemms_meeting_setup_detail,hemms_meeting_card_recode --outfile 'app/models.py' --flask
    # 反向遷移時改為:from extension import db
    SQLALCHEMY_DATABASE_URI = 'postgresql://FATP:FATP@10.41.158.153:5432/SABU_DB'
    SQLALCHEMY_TRACK_MODIFICATIONS = True
    # 静态资源加载问题
    # 方式一
    # app.jinja_env.auto_reload = True
    # 方式二每五秒请求静态资源
    from datetime import timedelta
    SEND_FILE_MAX_AGE_DEFAULT = timedelta(seconds=5)
    SQLALCHEMY_CLOSE_DEBUG = False


class TestingConfig(Config):
    TESTING = True