import multiprocessing
# 并行工作线程数
workers = multiprocessing.cpu_count() * 2 + 1
# 工作模式gevent sync
worker_class = 'gevent'
# 监听内网端口5000【按需要更改】
bind = '0.0.0.0:5000'
# 设置守护进程【关闭连接时，程序仍在运行】
# daemon = False

# preload_app = True
# reload = True

# 设置超时时间120s，默认为30s。按自己的需求进行设置
timeout = 600
# 设置访问日志和错误信息日志路径
accesslog = './logs/gunicorn/acess.log'
errorlog = './logs/gunicorn/error.log'
