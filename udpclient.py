#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   udpclient.py
@Time    :   2021/01/30 10:00:14
@Author  :   Z20080498 
@Version :   1.0
@Contact :   
@WebSite :  
'''
# Start typing your code from here

import socket
import struct


def deviceid_to_meetingroom(deviceid):
    """
    deviceid_to_meeting = {"12M1":["1331339211"]}  # 设备信息对应会议室
    输入设备id,返回会议室名称
    """
    deviceid_to_meetingroom = {"1331339211":"12M1"}
    for k,v in deviceid_to_meetingroom.items():
        if deviceid == int(k):
            return v
    return ''


def return_meeting_people(meeting):
    """
    meeting 会议对象query
    返回会议人员列表
    """
    sitinon = meeting.sitinon  # 列席人
    if sitinon:
        res = meeting.attendees.split(',') + sitinon.split(',')  # 所有会议人员的工号
    else:
        res = meeting.attendees.split(',')
    return res


def OpenTheDoor(deviceid):
    """
    調用udp開門指令

    param: deviceid 設備id,根據設備下發制定設備開門指令
    return {"msg":"ok"}
    """
    byte = b""
    address = ('10.41.141.112', 60000)

    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM) 

    byte += struct.pack('B',int('0x17',16))  # 類型0
    byte += struct.pack('B',int('0x40',16))  # 功能號1
    byte += struct.pack('H',int('0x0000',16))  # 保留2 3
  
    # iDevSn = '0x07ef7661'  # 133133921地位在前高位在后
    iDevSn = hex(int(deviceid[:-1]))  # 設備序列號4567
    byte += struct.pack('I',int(iDevSn,16))
    # 32位數據data 8-39
    data = f'{hex(int(deviceid[-1:]))}'+',0x00'*31
    for i in data.split(','):
        byte += struct.pack('B',int(i,16))
    # 數據包流水號  一对请求和应答消息的流水号相同
    byte += struct.pack('I',int('0x00000000',16))
    # 擴展20位
    extern_data = '00'+',00'*19
    for j in extern_data.split(','):
        byte += struct.pack('B',int(j,16))

    udp_socket.sendto(byte, address)  # 遠程開門指令
    udp_socket.close()
    return {"msg":"ok"}


def LockCard():
    """
    模擬刷卡數據上報
    """
    udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    # bytess = b'\x17 \x00\x00av\xef\x07S\x00\x00\x00\x01\x00\x01\x022H)\x00 !\x01(\x08\x02P\x06\x01\x00\x00\x00\x00\x00\x00\x00\x00\x08\x02P\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00!\x01(\x00\x00<*Y@\x10\x91\x00\x00'
    by = b'\x17 \x00\x00av\xef\x07\xca\x00\x00\x00\x01\x00\x01\x02\x90H\x1c\x00 !\x02\x05\x14\x00\x05\x06\x01\x00\x00\x00\x00\x00\x00\x00\x00\x14\x00\x05\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00!\x02\x05\x00\x00E*\x02p\x18\x91\x00\x00'
    # by = b'\x17 \x00\x00av\xef\x07R\x00\x00\x00\x01\x00\x01\x022H)\x00 !\x01(\x08\x02H\x06\x01\x00\x00\x00\x00\x00\x00\x00\x00\x08\x02H\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00!\x01(\x00\x00<*X@\x10\x91\x00\x00'
    udp_socket.sendto(by, ('10.41.208.10', 6000))
    print('发送完毕!')
    udp_socket.close()
    return {"msg":"ok"}













# 2:
# for i in range(len(str_list)):
#     # unsigned char
#     if i in ([0,1]+list(range(8,40))+list(range(44,64))):
#         print([0,1]+list(range(8,40))+list(range(44,64)))
#         byte += struct.pack('B',int(str_list[i],16))
#     # unsigned short
#     if i in [2,3]:
#         byte += struct.pack('H',int(str_list[i],16))
#     # unsigned int
#     if i in ([4,5,6,7]+list(range(40,44))):
#         print([4,5,6,7]+list(range(40,44)))
#         byte += struct.pack('I',int(str_list[i],16))



# 1:
# types = 0x17 # int('17',16))                  # 類型23
# functionID = 0x40 # int('40',16))            # 功能號64
# reserved = 0x0000                              # 保留
# iDevSn = 0x0D4AB63B  # 223000123                         # 設備序列號
# data = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'  # 32位字符串
# sequenceId = 0x0D4AB63B                             # 數據包流水號一位整形
# extern_data = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'       # 20位字符串

# fmt = '>2BHI32BI20B'
# print(struct.calcsize(fmt))
# byte = struct.pack(fmt,types,functionID,reserved,iDevSn,data,sequenceId,extern_data)