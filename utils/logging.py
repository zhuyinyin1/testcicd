import logging
from flask.logging import default_handler
import os
from logging.handlers import RotatingFileHandler
from logging import StreamHandler

# BASE_DIR = os.path.dirname(os.path.abspath(__file__))
BASE_DIR = os.getcwd()

LOG_PATH = os.path.join(BASE_DIR, 'logs')

LOG_PATH_ERROR = os.path.join(LOG_PATH, 'error_log.log')

LOG_PATH_ALL = os.path.join(LOG_PATH, 'std_log.log')
# 日志文件最大 10MB
LOG_FILE_MAX_BYTES = 10 * 1024 * 1024
# 轮转数量是 10 个
LOG_FILE_BACKUP_COUNT = 10

class Logger(object):
    """
    CRITICAL > ERROR > WARNING > INFO > DEBUG > NOTSET
    """
    def init_app(self, app):
        # 移除默认的handler
        app.logger.removeHandler(default_handler)
        # [%(thread)d:%(threadName)s] [%(filename)s:%(module)s:%(funcName)s]
        # %(asctime)s [%(levelname)s]: %(message)s
        # 定義日誌格式:
        formatter = logging.Formatter(
        '[%(asctime)s] [%(levelname)s] : %(message)s'
        )
        # 創建兩個日誌記錄器,分別將日誌輸出到文件和console

        
        # 1.將日誌輸出到日誌文件
        # 超过10MB自动开始写入新的日志文件，历史文件归档
        file_handler = RotatingFileHandler(filename=LOG_PATH_ALL,mode='a',
            maxBytes=LOG_FILE_MAX_BYTES,
            backupCount=LOG_FILE_BACKUP_COUNT,
            encoding='utf-8'
        )
        file_handler.setFormatter(formatter)  # 輸出格式
        file_handler.setLevel(logging.WARNING)

        # 2.將日誌輸出到console
        stream_handler = StreamHandler()
        stream_handler.setFormatter(formatter)  # 輸出格式
        stream_handler.setLevel(logging.DEBUG)


        for logger in (app.logger,logging.getLogger('sqlalchemy'),logging.getLogger('werkzeug')):
            logger.addHandler(file_handler)
            logger.addHandler(stream_handler)