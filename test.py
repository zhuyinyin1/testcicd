

# def get_host_ip():
#     """
#     查询本机ip地址
#     :return: ip
#     """
#     import socket
#     try:
#         s=socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
#         s.connect(('8.8.8.8',80))
#         ip=s.getsockname()[0]
#     finally:
#         s.close()
#     return ip

# print(get_host_ip())


# import datetime
# tt = datetime.datetime.now()
# print(tt,type(tt))
# print(tt.strftime('%Y-%m-%d %H:%M:%S'))
# import time
# print(time.time(),type(time.time()))

# CREATE TABLE "SFCPCB131".hemms_meeting_setup_detail_tmp (
# 	"sysid" varchar(18) NOT NULL,
# 	meetingid varchar(18) NOT NULL,
# 	seq int4 NOT NULL,
# 	starttime timestamp NOT NULL,
# 	endtime timestamp NOT NULL,
# 	status varchar(1) NULL,
# 	frequency int4 NOT NULL,
# 	plant varchar(4) NOT NULL,
# 	subject varchar(100) NOT NULL,
# 	meetingroom varchar(30) NOT NULL,
# 	host varchar(12) NOT NULL,
# 	attendees varchar(4000) NULL,
# 	sitinon varchar(4000) NULL
# ); 
# tt = {""}

# import datetime
# start_date = datetime.datetime.strptime('2021-01-19 08:50:10', "%Y-%m-%d %H:%M:%S")
# print(start_date)
# alerttime = datetime.timedelta(minutes=15)
# print(alerttime)

# print(start_date-alerttime)

# attendees = 'rrerer'
# p_list = attendees.split(',')

# sitinon = 'dwdw,444'
# print(p_list)

# t = attendees.split(',') + sitinon.split(',')
# print(t)


# r = '123ert'

# if '123' in r:
#     print(1)


# import socket

# udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# # 可以使用套接字收发数据
# #udp_socket.sendto(内容（必须是bytes类型）, 对方的ip以及port)
# udp_socket.sendto(b'0x170x580x000x000x3B0xB60x4A0x0D0x00', ('47.97.25.111', 6000))  # 发送UDP数据。将数据发送到套接字，address是形式为（ipaddr，port）的元组，指定远程地址。返回值是发送的字节数。
# udp_socket.recvfrom(1024)	# 接受UDP套接字的数据。与recv()类似，但返回值是（data,address）。
#                                     # 其中data是包含接收数据的字符串，address是发送数据的套接字地址。

# print('发送完毕!')
# # 关闭套接字
# udp_socket.close()


# import socket
import struct

# udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
# # udp_socket.sendto(内容（必须是bytes类型）, 对方的ip以及port)
# print('开始发送')

# # B	unsigned char	
# # H	unsigned short
# # I(大写的i)	unsigned int
# # +',00'*55
# strs = '17,40,00,00,3B,B6,4A,0D,01'

# byte = b""
# for i in strs.split(','):
#     byte += struct.pack('B',int(i,16))

# print(len(byte))
# print(byte)


# udp_socket.sendto(byte, ('10.41.208.10', 6000))

# print('发送完毕!')
# udp_socket.close()
# t = '0x17'
# print(int(t,16))
# print(int('0x000x00',16))

# print(int('0x00',16))
# byte = struct.pack(fmt,type,functionID)

# print(byte)
# t =str(int('0x0D4AB63B',16))
# print(t,type(t))

# t = [0,1]+list(range(8,32))+list(range(43,64))

# print(t)
# 0x32482900
# hex(16)      50 x32, 72  x48, 41 x29, 0 x00
# 50, 72, 41, 0   # 02705458
# print('0X20'.lower()) #  1635184391   0x6176ef07 10进制转16进制
# m = '12'
# month = int(m[1:]) if m[:1] == '0' else int(m)
# print(month)


# deviceid = '733833931'
# print(hex(int(deviceid[:-1])))
# print(int('0x7ef7661',16))
# print(hex(int(deviceid[-1:])))
# import time
# print(time.time.now())

# 功能號: 32
# 流水號: 0
# 刷卡數據上報
# (23, 32, 0, 133133921, 85, 1, 0, 1, 2, 2235020, 32, 33, 1, 40, 22, 8, 70, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 22, 8, 70, 0, 0, 0, 0, 0, 33, 1, 40, 0, 0, 60, 42, 23, 129, 16, 145, 0, 0)
# 卡號: 2235020
# {'notifyType': 'lockCard', 'requestId': '32', 'gatewayId': 85, 'deviceId': '1331339211', 'serviceType': 'nan', 'eventTime': 1611821326, 'service': {'data': '02235020'}}
# 開始監聽數據
# 功能號: 32
# 流水號: 0
# 刷卡數據上報
# (23, 32, 0, 133133921, 86, 1, 0, 1, 2, 3657925, 32, 33, 1, 40, 22, 8, 86, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 22, 8, 86, 0, 0, 0, 0, 0, 33, 1, 40, 0, 0, 60, 42, 28, 129, 16, 145, 0, 0)
# 卡號: 3657925
# {'notifyType': 'lockCard', 'requestId': '32', 'gatewayId': 86, 'deviceId': '1331339211', 'serviceType': 'nan', 'eventTime': 1611821336, 'service': {'data': '03657925'}}
# 開始監聽數據
# 功能號: 32
# 流水號: 0
# 刷卡數據上報
# (23, 32, 0, 133133921, 87, 1, 1, 1, 2, 12547859, 32, 33, 1, 40, 22, 8, 89, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 22, 8, 89, 0,
# 0, 0, 1, 0, 33, 1, 40, 0, 0, 60, 42, 29, 129, 24, 1, 0, 0)
# 卡號: 12547859
# {'notifyType': 'lockCard', 'requestId': '32', 'gatewayId': 87, 'deviceId': '1331339211', 'serviceType': 'nan', 'eventTime': 1611821339, 'service': {'data': '12547859'}}
# 開始監聽數據
# 功能號: 32
# 流水號: 0
# 刷卡數據上報
# (23, 32, 0, 133133921, 88, 1, 0, 1, 2, 2705458, 32, 33, 1, 40, 22, 9, 4, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 22, 9, 4, 0, 0, 0, 0, 0, 33, 1, 40, 0, 0, 60, 42, 34, 129, 16, 145, 0, 0)
# 卡號: 2705458
# {'notifyType': 'lockCard', 'requestId': '32', 'gatewayId': 88, 'deviceId': '1331339211', 'serviceType': 'nan', 'eventTime': 1611821344, 'service': {'data': '02705458'}}
# 開始監聽數據
# 功能號: 32
# 流水號: 0
# 刷卡數據上報
# (23, 32, 0, 133133921, 89, 1, 0, 1, 2, 1853584, 32, 33, 1, 40, 22, 9, 7, 6, 1, 0, 0, 0, 0, 0, 0, 0, 0, 22, 9, 7, 0, 0, 0, 0, 0, 33, 1, 40, 0, 0, 60, 42, 35, 129, 24, 145, 0, 0)
# 卡號: 1853584
# {'notifyType': 'lockCard', 'requestId': '32', 'gatewayId': 89, 'deviceId': '1331339211', 'serviceType': 'nan', 'eventTime': 1611821347, 'service': {'data': '01853584'}}


# print(set(['zzz','123','222','zzz']))

print(int('0'))