from flask import Flask, request, render_template, send_from_directory
# 前台蓝图
from app.web.views import web_bpt
from config import settings


def create_app():
    app = Flask(__name__)
    # 注册蓝图
    app.register_blueprint(blueprint=web_bpt, url_prefix='/') # 前台藍圖
    # 导入配置文件
    # app.config.from_object(settings.ProductionConfig)  # 生產環境
    app.config.from_object(settings.DevelopmentConfig)  # 開發環境

    return app
