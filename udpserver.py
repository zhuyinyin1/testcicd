#!/usr/bin/env python
# -*- encoding: utf-8 -*-
'''
@File    :   udpserver.py
@Time    :   2021/01/30 09:59:56
@Author  :   Z20080498 
@Version :   1.0
@Contact :  
@WebSite :  
'''
# Start typing your code from here

import socket
import requests,struct
import datetime,time
import binascii


if __name__ == '__main__':
    # unsigned char	 type;				  //类型              1     B             B
	# unsigned char	 functionID;		      //功能号        1     B           B
	# unsigned short	 reserved;              //保留        2     H           2H
	# unsigned int	 iDevSn;               //设备序列号 4字节   4    I          4I
    # unsigned char  data[32];              //32字节的数据      1    B               32B
    # unsigned int   sequenceId;            //数据包流水号     4     I          4I
    # unsigned char  extern_data[20];        //扩展20字节      1     B          20B
    send_to_url = 'http://127.0.0.1:5000/lockcard/'
    ip_can_list = ['10.41.141.112','10.41.208.10']
    while True:
        print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] msg[開始監聽數據]')
        udp_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        bind_addr = ("0.0.0.0", 6000)
        udp_socket.bind(bind_addr)
        (recv_data,address) = udp_socket.recvfrom(64)
        if address[0] in ip_can_list:
            fmts = '<BBHII4BI20BII16B'  # 開門解析
            # 先解析功能號,再根據功能號使用不同的fmt重新解析'<BBHI32BI20B'
            # functionID = struct.unpack(fmts, recv_data)[1]
            # sequenceId = struct.unpack(fmts, recv_data)[37]
            # print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] ip[{address}] 功能號[{functionID}] 流水號[{sequenceId}]')
            # 流水號為空,否則可能和查詢控制器狀態功能混合
            # if functionID == 32 and sequenceId == 0:  # 0x20 刷卡數據上報
            types = struct.unpack(fmts, recv_data)[5] # 記錄類型0無記錄1刷卡記錄2門磁按鈕(遠程開門)3報警記錄
            print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] ip[{address}] 記錄類型[{types}] 字節數據[{recv_data}')
            print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] ip[{address}] 記錄類型[{types}] 十六進制數據[{binascii.b2a_hex(recv_data)}')
            if types == 1: # 刷卡類型
                #   類型 功能號 保留 I序列號 I索引 4B 卡號 20B I流水 I備用 16B
                fmts = '<BBHII4BI20BII16B'  # 開門解析
                gatewayId = struct.unpack(fmts, recv_data)[4]
                deviceId = f'{struct.unpack(fmts, recv_data)[3]}{struct.unpack(fmts, recv_data)[7]}'
                y1 = hex(struct.unpack(fmts, recv_data)[10]).lower()[2:]
                y2 = hex(struct.unpack(fmts, recv_data)[11]).lower()[2:]
                year = int(y1+y2)
                m = hex(struct.unpack(fmts, recv_data)[12]).lower()[2:]  # 0x20 去掉0x
                month = int(m[1:]) if m[:1] == '0' else int(m)
                d = hex(struct.unpack(fmts, recv_data)[13]).lower()[2:]
                day = int(d[1:]) if d[:1] == '0' else int(d)
                h = hex(struct.unpack(fmts, recv_data)[14]).lower()[2:]
                hour = int(h[1:]) if h[:1] == '0' else int(h)
                mi = hex(struct.unpack(fmts, recv_data)[15]).lower()[2:]
                minute = int(mi[1:]) if mi[:1] == '0' else int(mi)
                s = hex(struct.unpack(fmts, recv_data)[16]).lower()[2:]
                second = int(s[1:]) if s[:1] == '0' else int(s)
                eventTimeStr = datetime.datetime(year,month,day,hour,minute,second).strftime("%Y-%m-%d %H:%M:%S")
                timeArray = time.strptime(eventTimeStr, "%Y-%m-%d %H:%M:%S") # 转为时间数组
                # 转为时间戳
                eventTime = int(time.mktime(timeArray))
                cid = str(struct.unpack(fmts, recv_data)[9])
                if len(cid) < 8:  # 八位,不足八位的前面補0
                    cardid = '0'*(8-len(cid)) + cid
                else:
                    cardid = struct.unpack(fmts, recv_data)[9]
                res = {
                    "notifyType": "lockCard",
                    "requestId": f"{types}",  # 記錄類型
                    "gatewayId": gatewayId,  # 消息序列號
                    "deviceId": deviceId,  # 設備id加門號
                    "serviceType": "nan",
                    "eventTime": eventTime,  # 刷卡时间
                    "service": {
                    "data": f"{cardid}"  # 卡号
                    }
                }
                result = requests.post(send_to_url,json=res)
                if result.status_code == 200:
                    print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] msg[已成功發送刷卡數據:{res}]')
                else:
                    print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] msg[刷卡數據發送失敗]')

            # elif functionID == 64: # 0x40 遠程開門
            elif types == 2: # 遠程開門記錄(注意可能按鈕也會觸發此返回值)
                fmts = '<BBHII4BI20BII16B'
                print(f'時間[{datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")}] msg[遠程開門]')
        # 关闭套接字
        udp_socket.close()
