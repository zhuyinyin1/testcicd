FROM python:3
LABEL name = 'YannZhu'
WORKDIR /var/www/wistron/
COPY . .
RUN pip install --no-cache-dir -r requirements.txt
EXPOSE 5000
ENTRYPOINT [ "gunicorn", "manage:app" ]
CMD ["-c", "config/gunicorn.conf.py"]
